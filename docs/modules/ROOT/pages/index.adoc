= Top Page

== Who am I ?

氏名::
  三戸圭史
経歴::
  筑波大学大学院コンピュータサイエンス専攻 2020年3月 修了
専攻::
  統計的機械学習
興味関心::
  * ベイズ推論
  * ガウス過程
  * 表現学習
  * Julialang
  * 競技プログラミング
  * NLP
実績（査読ありジャーナル）::
  * Keishi Sando, Hideitsu Hino, "Modal Principal Component Analysis", Neural Computation, 2020.10
  * Keishi Sando, Shotaro Akaho, Noboru Murata, Hideitsu Hino, "Information Geometry of Modal Linear Regression", Information Geometry, 2019.6
実績（査読あり国際学会）::
  * Keishi Sando, Shotaro Akaho, Noboru Murata, Hideitsu Hino, “Information Geometric Perspective of Modal Linear Regression”, The 25th International Conference on Neural Information Processing (ICONIP2018), 2018.12 (Siem Reap, Cambodia)
実績（査読なし国内研究会）::
  * 三戸圭史，日野英逸，“最頻値推定量を用いた主成分分析の提案”，第39回情報論的学習理論と機械学習研究会（IBISML），2020.3（京都），※1
  * 三戸圭史，赤穂昭太郎，村田昇，日野英逸，“最頻値線形回帰の情報幾何”，第32回情報論的学習理論と機械学習研究会 (IBISML)，2018.3（九州）
受賞歴::
  * 2019年度 IBISML研究会賞 ファイナリスト(※1)
資格::
  * [2020.10.05] AWS認定デベロッパーアソシエイト
その他::
  * link:https://atcoder.jp/users/keisan[競技プログラミング活動]

== Contents

* [2020.11.24] AsciiDocによるドキュメント作成の link:https://keishi-sandbox.s3-ap-northeast-1.amazonaws.com/intro.adoc/index.html[ススメ]
* [2020.11.18] 数式を含むadocファイルを綺麗にpdf出力するための link:https://qiita.com/nobuta05/items/3cd1add1251e4ed1a274[方法]
* [2020.11.15] デジタル化中の確率論関連の link:https://keishi-sandbox.s3-ap-northeast-1.amazonaws.com/mathproofs.pdf[資料]。作成用の link:https://gitlab.com/nobuta05/mathproofs[リポジトリ]
* [2020.11.09] 最適化ソルバーのリハビリを兼ねた数独求解の link:https://gist.github.com/nobuta05/6bdfb021caaa097429bea75e7df487a0[実装メモ]
* [2020.10.18] 状態遷移図作成の link:https://gist.github.com/nobuta05/dc3ba94391c46d713f618a98ac4444d1[メモ書き]
* [2020.10.18] シーケンス図作成の link:https://gist.github.com/nobuta05/ca6738f524e1957d00348d95c340a8de[メモ書き]
* [2020.07.05] 基本的なGPLVMの link:https://nbviewer.jupyter.org/gist/nobuta05/9499b9d5ece86a23b86b741727d8c33e[実装メモ]
* [2020.06.28] Scaled Conjugate Gradientの link:https://nbviewer.jupyter.org/gist/nobuta05/85c6e5087ad25db644f93da6d3f7be68[実装メモ]
* J.O. Ramsay, B.W. Silverman, "Functional Data Analysis", Chapter 7のphase variationの link:https://nbviewer.jupyter.org/gist/nobuta05/89db4491c273fd74490646c8e4c36355[計算メモ]
* JulialangのPlotライブラリの1つ，PGFPlotsXの link:https://nbviewer.jupyter.org/gist/nobuta05/edc28f89d290443c72e68c1d314ddea3/PGFPlotsX_Examples.ipynb[Example集]
* JulialangのPlotライブラリの1つ、PlotlyJSの link:https://nbviewer.jupyter.org/gist/nobuta05/235e7618028969f9d4a03ab89cc6303f[Example集]。特徴的なのは、jupyterlab上では3dグラフをグリグリ動かせること。
* Julialangの link:https://nbviewer.jupyter.org/gist/nobuta05/98b9ba4503e0d55e8e663bf656c14bad[Tips]
* 語学学習特化の音声ファイル再生androidアプリ link:https://play.google.com/store/apps/details?id=site.nobuta05.player4l[Player4L]